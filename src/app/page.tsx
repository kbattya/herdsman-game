import Image from "next/image";
import styles from "./page.module.css";
import GameField from "./client/GameField";


export default function Home() {
  return (
    <main>
      <GameField />
    </main>
  );
}

export interface Animal {
  id: number;
  x: number;
  y: number;
  bind: boolean;
}

export interface Position {
  x: number;
  y: number;
}

export interface HeroProps {
  position: Position;
  moveHero: (position: Position) => void;
}
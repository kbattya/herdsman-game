'use client'

import React, { useState, useEffect, useRef } from "react";
import { generateRandom } from '../helpers/helpers';
import useWindowDimensions from "../helpers/useDimensions";

import { Position } from "../models/models";
import { Animal } from "../models/models";
import Hero from "./Hero";

const GameField: React.FC = () => {
  const [score, setScore] = useState<number>(0);
  const [keys, setKeys] = useState<Record<string, boolean>>({});
  const [heroPosition, setHeroPosition] = useState<Position>({ x: 500, y: 500 });
  const [groupPosition, setGroupPosition] = useState<Position>({ x: 500, y: 500 });
  const [animals, setAnimals] = useState<Animal[]>([]);
  const [caughtAnimals, setCaughtAnimals] = useState<Animal[]>([]);
  const gate = useRef<HTMLDivElement | null>(null);
  const group = useRef<HTMLDivElement | null>(null);
  const { height, width } = useWindowDimensions();

  useEffect(() => {
		if (width && height) {
			const initialAnimals = Array.from({ length: 13 }, (_, index) => ({
				id: index + 1,
				x: generateRandom(100, width - 100),
				y: generateRandom(100, height - 100),
				bind: false
			}));
	
			setAnimals(initialAnimals);
		}
    
  }, [width, height]);

  useEffect(() => {
    const onClickMove = (e: MouseEvent) => {
      setHeroPosition({ x: e.clientX, y: e.clientY });
    };

    const keyDown = (e: KeyboardEvent) => {
      setKeys((prev) => ({ ...prev, [e.key]: true }));
    };

    const keyUp = (e: KeyboardEvent) => {
      setKeys((prev) => ({ ...prev, [e.key]: false }));
    };

    document.addEventListener("click", onClickMove);
    document.addEventListener("keydown", keyDown);
    document.addEventListener("keyup", keyUp);

    return () => {
      document.removeEventListener("click", onClickMove);
      document.removeEventListener("keydown", keyDown);
      document.removeEventListener("keyup", keyUp);
    };
  }, []);

  useEffect(() => {
    const interval = setInterval(() => {
      gameLoop();
    }, 20);

    return () => clearInterval(interval);
  }, [keys]);

  useEffect(() => {
    const moveGroup = setInterval(() => followHero(), 20);

    setAnimals((animals) => animals.map((a) => updateAnimal(a)));

    if (
      gate.current &&
      heroPosition.x >= gate.current.offsetLeft &&
      heroPosition.x <= gate.current.offsetLeft + gate.current.offsetWidth &&
      heroPosition.y >= gate.current.offsetTop &&
      heroPosition.y <= gate.current.offsetTop + gate.current.offsetHeight
    ) {
      setScore((prev) => prev + animals.filter((an) => an.bind).length);
      setAnimals((prev) => prev.filter((an) => !an.bind));
    }

    return () => clearInterval(moveGroup);
  }, [heroPosition]);

  useEffect(() => {
    setCaughtAnimals(animals.filter((an) => an.bind));
  }, [animals]);

  const isAnimalClose = (animal: Animal): boolean => {
    return (
      heroPosition.x - animal.x < 60 &&
      heroPosition.x - animal.x > -60 &&
      heroPosition.y - animal.y < 60 &&
      heroPosition.y - animal.y > -60
    );
  };

	const updateAnimal = (a:Animal) => {
		if (isAnimalClose(a) && (animals.filter((an) => an.bind).length < 5 || a.bind)) {
			return { ...a, x: heroPosition.x - 10, y: heroPosition.y - 10, bind: true };
		} else {
			return a
		}
	}

  const gameLoop = () => {
    if (
      heroPosition.y - 40 > 0 &&
      heroPosition.x - 40 > 0 &&
      heroPosition.y + 40 < height &&
      heroPosition.x + 40 < width
    ) {
      if (keys["w"] || keys["ArrowUp"]) {
        setHeroPosition((prev) => ({ x: prev.x, y: prev.y - 5 }));
      }

      if (keys["a"] || keys["ArrowLeft"]) {
        setHeroPosition((prev) => ({ x: prev.x - 5, y: prev.y }));
      }

      if (keys["s"] || keys["ArrowDown"]) {
        setHeroPosition((prev) => ({ x: prev.x, y: prev.y + 5 }));
      }

      if (keys["d"] || keys["ArrowRight"]) {
        setHeroPosition((prev) => ({ x: prev.x + 5, y: prev.y }));
      }
    }
  };

	const followHero = () => {
		if (group.current &&
			(heroPosition.x < group.current.offsetLeft ||
				heroPosition.x > group.current.offsetLeft + group.current.offsetWidth ||
				heroPosition.y < group.current.offsetTop ||
				heroPosition.y > group.current.offsetTop + group.current.offsetHeight
			)) {
			setGroupPosition((prev) => ({
				x: prev.x + (heroPosition.x - prev.x) * 0.1,
				y: prev.y + (heroPosition.y - prev.y) * 0.1,
			}));
		}
	}

  return (
    <div className="field">
      <div className="score">Score: {score}</div>
			<Hero position={heroPosition} moveHero={setHeroPosition} />

      <div
        className="group"
        ref={group}
        style={{
          position: "relative",
          left: groupPosition.x + 'px',
          top: groupPosition.y + 'px'
        }}
      >
        {caughtAnimals?.length > 0 && caughtAnimals.map((animal, index) => (
          <div
            className="animal"
            key={animal.id}
            style={{
              position: "absolute",
              zIndex: 5,
              left: index * 15 + '%',
            }}
          ></div>
        ))}
      </div>

      {animals.map((animal) => (
        <div
          className="animal"
          key={animal.id}
          style={{
            position: "absolute",
            left: animal.x + 'px',
            top: animal.y + 'px',
            display: animal.bind ? 'none' : 'block',
          }}
        ></div>
      ))}

			<div className="gate" ref={gate}></div>
    </div>
  );
};

export default GameField;
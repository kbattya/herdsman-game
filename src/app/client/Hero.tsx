'use client'

import React, { useEffect } from "react";
import { HeroProps } from "../models/models";

const Hero: React.FC<HeroProps> = ({ position, moveHero }) => {
  const onClickMove = (e: MouseEvent) => {
    moveHero({ x: e.clientX, y: e.clientY });
  };

  useEffect(() => {
    document.addEventListener("click", onClickMove);
    return () => {
      document.removeEventListener("click", onClickMove);
    };
  }, []);

  return (
    <div
      className="hero"
      style={{
        position: "absolute",
        left: position.x + 'px',
        top: position.y + 'px'
      }}
    ></div>
  );
};

export default Hero